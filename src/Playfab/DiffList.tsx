import React from "react";
import localStorageManager from "../localStorageManager";
import {Segment} from "semantic-ui-react";
import neckwear from "./neckwear.json"

interface MyProps {
    playfabFile: any | undefined
}

interface MyState {
    output: Array<JSX.Element>
}

interface luaItemData {
    class: string,
    rarity: number,
    file?: string,
    group?: number,
    description?: string,
    link?: string,
    hidden?: number,
    name?: string,
    framed?: number
}

interface JSCustomData {
    Rarity: string,
    SS?: number,
    DNA_Fox?: number
}

interface JSCatalogEntry {
    Tags: Array<string>,
    CustomData: string,
    ItemClass: string,
    DisplayName: string,
    Rarity?: string,
    ItemId: string
}

const rarities: {[index: number]:string} = {0: "common", 1: "uncommon", 2: "rare", 3: "epic", 4: "legendary", 5: "special"};
const framed: {[index: number]:string} = {0: "unframed", 1: "framed"};

class DiffMod extends React.Component<MyProps, MyState> {
    constructor(prop: any) {
        super(prop);

        this.state = {
            output: []
        };
    }

    changeItemAttribute(itemName: string, attributeName: string, newValue: any) {
        let localStorage: {[index: string]: luaItemData} = localStorageManager.items.read();
        if (newValue === false) {
            // @ts-ignore
            delete localStorage[itemName][attributeName]
        } else {
            // @ts-ignore
            localStorage[itemName][attributeName] = newValue;
        }
        localStorageManager.items.overwrite(localStorage);
        // localStorageManager.items.save();
    }

    createDefaultNewItem(item: JSCatalogEntry, itemName: string): JSX.Element { // this.changeItemAttribute(itemName, "hidden", e.target.value === "on");
        return (
            <div className="new-item" key={item.DisplayName}>
                <span className="item-name">{itemName}</span>
                <label title="Add the item to the final output">Add
                    <input type="checkbox" onChange={(e) => {this.switchItemState(item, e.target.value === "on")}}/>
                </label><span className="item-class">{item.ItemClass}</span></div>
        )
    }

    spewHTMLofChanges(value1: number | string, value2: number | string, translationValues: {[index: number]:string} | null, key: string): JSX.Element {
        if (translationValues === null) {
            return (<div className="changes" key={key}><span className="old-value">{value1}</span><span className="arrow">{" 🡆 "}</span><span className="new-value">{value2}</span></div> )
        }
        // @ts-ignore
        return (<div className="changes" key={key}><span className="old-value">{translationValues[value1]}</span><span className="arrow">{" 🡆 "}</span><span className="new-value">{translationValues[value2]}</span></div> )
    }

    switchItemState(itemData: JSCatalogEntry, newState: boolean) {
        if (newState) {
            this.addItemToStorage(itemData)
        } else {
            this.removeItemFromStorage(itemData.DisplayName)
        }
    }

    addItemToStorage(itemData: JSCatalogEntry) {
        let localStorage: {[index: string]: luaItemData} = localStorageManager.items.read();
        localStorage[itemData.DisplayName] = {class: itemData.ItemClass, rarity: itemData.Rarity === undefined ? 0 : parseInt(itemData.Rarity)}
        if (itemData.Tags.includes("vip")) {
            localStorage[itemData.DisplayName].framed = 1
        }
        localStorageManager.items.overwrite(localStorage);
    }

    removeItemFromStorage(itemName: string) {
        let localStorage: {[index: string]: luaItemData} = localStorageManager.items.read();
        let item: luaItemData = localStorage[itemName];
        this.state.output.push(
            (<div className="removed-item"><span className="item-name">{itemName}</span></div> )
        )
        delete localStorage[itemName];
        localStorageManager.items.overwrite(localStorage);
    }

    compareItems(itemOld: luaItemData, itemNew: JSCatalogEntry, itemName: string): boolean {
        let changed = [];
        // framed in lua, vip in Tags property in json
        if ((itemOld.framed !== undefined ? 1 : 0) !== (itemNew.Tags.includes("vip") ? 1 : 0)) {
            changed.push(this.spewHTMLofChanges(itemOld.framed !== undefined ? 1 : 0, itemNew.Tags.includes("vip") ? 1 : 0, framed, itemName+"difff"))
        }
        // rarity in lua, Rarity in CustomData in json
        if (itemNew.CustomData === undefined) {
            return false;
        }
        let customData: JSCustomData = JSON.parse(itemNew.CustomData);
        // @ts-ignore eslint-disable-next-line eqeqeq
        if (itemOld.rarity != customData.Rarity) {
            changed.push(this.spewHTMLofChanges(itemOld.rarity, parseInt(customData.Rarity), rarities, itemName+"diffr"))
        }
        if (neckwear.indexOf(itemNew.ItemId) != -1) {
            itemNew.ItemClass = "Neck";
        }
        if (itemOld.class != itemNew.ItemClass) {
            changed.push(this.spewHTMLofChanges(itemOld.class, itemNew.ItemClass, null, itemName+"diffc"))
        }
        if (changed.length > 0) {
            console.log("Changed item: " + itemName)
            this.state.output.push(
                (
                    <div className="changedItem" key={itemName}><span className="item-name">{itemOld.name !== undefined ? itemOld.name : itemName}</span>{changed}</div>
                )
            )
            return true;
        } else {
            return false;
        }
    }

    generateDiff() {
        let data = this.props.playfabFile;
        if (data.data !== undefined) {
            data = data.data;
        }
        if (data.Catalog !== undefined) {
            data = data.Catalog;
        }
        console.log(data);
        if (!(Array.isArray(data))) {
            alert("Couldn't load the data. Please check the console for more information.")
            console.log("Couldn't load the data. Data is not an array: ");
            console.log(data);
            return
        }
        let localStorage: {[index: string]: luaItemData} = localStorageManager.items.read();
        let allExistingItems = Object.keys(localStorage);
        for (const item of data) {
            let itemNo = allExistingItems.indexOf(item.DisplayName);
            if (item.CustomData === undefined) {
                continue;
            }
            let customData: JSCustomData | undefined = undefined;
            try {
                customData = JSON.parse(item.CustomData);
            }
            catch (e) {
                if (typeof e === "string") {
                    console.log(e.toUpperCase()+" on "+item.ItemId) // works, `e` narrowed to string
                } else if (e instanceof Error) {
                    console.log(e.message);
                    console.log("on "+item.ItemId)
                }
                throw e;
            }
            if (customData === undefined) {
                continue;
            }
            if (itemNo === -1) {
                if (item.DisplayName === undefined || item.DisplayName === "None") {
                    continue;
                }
                console.log("New item: "+item.DisplayName);
                item.Rarity = customData.Rarity;
                this.state.output.push(this.createDefaultNewItem(item, item.DisplayName));
            } else {
                allExistingItems.splice(itemNo, 1);
                if (this.compareItems(localStorage[item.DisplayName], item, item.DisplayName)) {
                    localStorage[item.DisplayName].rarity = parseInt(customData.Rarity);
                    if (item.Tags.includes("vip")) {
                        localStorage[item.DisplayName].framed = 1;
                    }
                    localStorage[item.DisplayName].class = neckwear.indexOf(item.ItemId) != -1 ? "Neck" :item.ItemClass;
                }
            }
        }
        for (const item of allExistingItems) {

        }
        localStorageManager.items.overwrite(localStorage);
        // localStorageManager.items.save();
        this.forceUpdate();
    }

    componentDidUpdate(prevProps: Readonly<MyProps>, prevState: Readonly<MyState>, snapshot?: any) {
        if (prevProps.playfabFile === undefined && this.props.playfabFile !== undefined) {
            this.generateDiff();
        }
    }

    render() {
        if (this.props.playfabFile) {
            return (
                <Segment key="segment-of-output" className="change-list">
                    <h2>Differences</h2>
                    <div key="main-div-output">{this.state.output}</div>
                </Segment>
            )
        } else {
            // @ts-ignore
            if (((new Date()) - new Date(localStorageManager.updateDate.read())) > 7200000) {
                return (
                    <div className="warning"><span className="warning">Please update the last ItemList revision from the wiki.</span></div>
                )
            } else {
                return (
                    <div>
                        <span>Please select a file above.</span>
                    </div>
                )
            }
        }
    }
}


export default DiffMod;