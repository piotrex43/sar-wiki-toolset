import React from "react";
import {Link} from "react-router-dom";
import "./Playfab.css";
import ExportLuaModule from "../Wiki-mods/exportModule";
import DiffMod from "./DiffList";
import makeAPIrequest from "../MediaWikiRequests";
import localStorageManager from "../localStorageManager";

const { parse } = require('lua-json');
// interface MyState {
//     file: Blob | undefined,
//     jsonNew: object | undefined
// }

interface MyProps {
}

interface MyState {
    modalState: boolean,
    file: Blob | undefined,
    jsonNew: object | undefined
}

class Playfab extends React.Component<MyProps, MyState> {
    constructor(prop: any) {
        super(prop);
        this.state = {
            modalState: false,
            file: undefined,
            jsonNew: undefined
        };
        this.getModalClose = this.getModalClose.bind(this);
    }

    getModalClose() {
        this.setState({modalState: false});
    }

    makeRequest() {
        makeAPIrequest("GET", {
            action: "query",
            format: "json",
            prop: "revisions",
            titles: "Module:ItemList",
            rvprop: "content",
            rvslots: "main",
            rvlimit: "1"
        }).then((response) => {
            let data = parse(response.data.query.pages[Object.keys(response.data.query.pages)[0]].revisions[0].slots.main["*"]);
            if (localStorageManager.items.read() !== data) {
                localStorageManager.items.overwrite(data);
                localStorageManager.updateDate.overwrite();
                localStorageManager.items.save();
            }
        }).catch(function (error) {
            console.log(error);
        })
    }

    render() {
        return (
            <div>
                <div className="header-playfab">
                    <div className="header-playfab-name"><Link to="/"><button className="ui button">SAR Wiki Toolset</button></Link></div>
                    <div className="header-file">
                        <input type="file" name="file" accept=".json,.txt" onChange={(e) => {
                            if (e.target.files !== null) {
                                this.setState({file: e.target.files[0]});
                            }
                        }}/>
                        <div>
                            <button onClick={(e) => {
                                console.log(this.state.file);
                                const reader = new FileReader();
                                reader.onload = async (e) => {
                                    if (e.target !== null) {
                                        const text = (e.target.result)
                                        if (typeof text === "string") {
                                            this.setState({'jsonNew': JSON.parse(text)})
                                        }
                                    }
                                };
                                reader.readAsText(this.state.file as Blob);
                            }}>Show differences</button>
                        </div>
                    </div>
                    <div className="update-module">
                        <button onClick={this.makeRequest}>Update</button>
                    </div>
                    <div>
                        <button className="ui positive button" onClick={(e) => this.setState({modalState: true})}>Save data</button>
                    </div>
                </div>
                <DiffMod playfabFile={this.state.jsonNew}/>
                <ExportLuaModule getModalClose= {this.getModalClose} currentState={this.state.modalState}/>
            </div>
        )
    }
}

// class Playfab extends React.Component<MyProps, MyState> {
//     constructor(props: any) {
//         super(props);
//         this.state = {
//             file: undefined,
//             jsonNew: undefined
//         }
//     }
//
//
//     render() {
//         return (
//             <div>
//                 <input type="file" name="file" accept=".json,.txt" onChange={(e) => {
//                     if (e.target.files !== null) {
//                         this.setState({file: e.target.files[0]});
//                     }
//                 }}/>
//                 <div>
//                     <button onClick={(e) => {
//                         console.log(this.state.file);
//                         const reader = new FileReader();
//                         reader.onload = async (e) => {
//                             if (e.target !== null) {
//                                 const text = (e.target.result)
//                                 if (typeof text === "string") {
//                                     this.setState({'jsonNew': JSON.parse(text)})
//                                 }
//                             }
//                         };
//                         reader.readAsText(this.state.file as Blob);
//                     }}>Submit</button>
//                 </div>
//             </div>
//         )
//     }
// }

export default Playfab;
