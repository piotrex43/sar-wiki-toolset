import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCog} from "@fortawesome/free-solid-svg-icons";
import React from "react";
import Settings from "./Settings";
import "./Home.css";

const Home = () => {
    const settings_button = (
        <button className="ui icon button settings">
            <FontAwesomeIcon icon={faCog}/>
        </button>)

    function MenuElement(text: string, url: string)  {
        return <a href={url}><button className="fluid ui button">{text}</button></a>
    }

    return (
        <div className="homeHeader">
            <h2 className="welcome">Welcome on SAR Wiki Toolset</h2>
            <Settings button={settings_button}/>
            <h3>What would you want to do today?</h3>
            <div className="ui vertically divided grid limitWidth">
                <div className="one column row">
                    <div className="column">
                        {MenuElement("Get PlayFab data", "/sar/playfab")}
                    </div>
                </div>
                <div className="one column row">
                    <div className="column">
                        {MenuElement("Manage current wiki data", "/sar/wiki-data")}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Home;