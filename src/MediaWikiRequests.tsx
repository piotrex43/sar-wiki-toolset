import localStorageManager from "./localStorageManager";
import {AxiosResponse} from "axios";
const axios = require('axios').default;

const url = `https://animalroyale.fandom.com/${localStorageManager.settings.read().language === "en" ? "" : `${localStorageManager.settings.read().language}/`}api.php`

const session = axios.create({
    withCredentials: false,
    headers: {
        "User-Agent": "SAR Wiki Toolsetv1"
    }
})

interface paramsObject {
    [key: string]: string;
}

const makeAPIrequest = (method: string, params: paramsObject): Promise<AxiosResponse> => {
    params.origin = "*";
    return session.request({
        method: method,
        url: url,
        params: params
    });
}

// This has been created before I realized that I cannot make authenticated requests to MediaWiki Fandom wiki because
// of CORS restrictions. RIP in pepperoni initial login code [*]
// const logIntoWiki = () => {
//     let settings = localStorageManager.settingsController.read()
//     let login = settings.wikiLogin;
//     let password = settings.wikiPassword;
//     let loginToken: string = ""
//     makeAPIrequest("GET", {
//         action: "query",
//         meta: "tokens",
//         type: "login"
//     }).then((response) => {
//         loginToken = response.data.query.tokens.logintoken;
//     }).catch((error) => {
//         console.log("Couldn't download token due to error: ");
//         console.log(error);
//         return false
//     })
//     makeAPIrequest("POST", {
//         action: "login",
//         lgname: login,
//         lgpassword: password,
//         lgtoken: loginToken
//     }).then((response) => {
//         console.log(response.data);
//     }).catch((error) => {
//         console.log(error);
//     })
//
// }

export default makeAPIrequest;

