import React from "react";
import {Dimmer, Loader } from "semantic-ui-react";
import makeAPIrequest from '../MediaWikiRequests';
import Fuse from 'fuse.js'
import SearchItem from "./SearchItem";
import localStorageManager from "../localStorageManager";
import failed from "../images/failed.png";

const CryptoJS = require("crypto-js");
const { parse } = require('lua-json');


interface MyProps {
    term: string
}

interface MyState {
    content: rawTable,
    brokenImgs: rawTable,
    selectedImg: string,
    modal_open: boolean,
    forced_currency_windows: string[],
    load_items: number
}

interface currency {
    name: string,
    amount: number
}

interface itemData {
    name: string,
    custom_name?: string,
    image_url: string,
    class: string,
    rarity: number,
    link?: string,
    description?: string,
    extra?: string,
    hidden?: boolean,
    group?: number,
    event?: string,
    currency?: currency,
    currencies?: currency[],
    bundle?: string,
    shop?: "saw" | "carl"
}

interface rawTable {
    [index: string]: any
}

function getColor(color: number) {
    return { 0: 'gray-rar', 1: 'green-rar', 2: 'blue-rar', 3: 'purple-rar', 4: 'gold-rar', 5: 'orange-rar'}[color]
}


// /api.php?action=query&format=json&prop=imageinfo&list=&generator=search&iiprop=timestamp%7Cuser%7Curl&gsrsearch=Goat&gsrnamespace=6&gsrlimit=10
class ItemGallery extends React.Component<MyProps, MyState> {
    constructor(prop: any) {
        super(prop);
        this.state = {
            content: {},
            brokenImgs: {},
            selectedImg: "",
            modal_open: false,
            forced_currency_windows: [],
            load_items: 100
        };
        this.setNewModalState = this.setNewModalState.bind(this);
        this.changeFileProperties = this.changeFileProperties.bind(this);
    }

    componentDidMount() {
        this.makeRequest();
        const body = document.body;
        const html = document.documentElement;
        window.addEventListener('scroll', () => {
           if ((window.innerHeight + window.scrollY) >=  Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight)) {
                this.setState({load_items: this.state.load_items+100})
           }
        });
        localStorageManager.updateDate.overwrite();
    }

    componentWillUnmount() {
        // Technically a memory leak if we don't remove EventListener but it would be effort so shhh
        // window.removeEventListener('scroll', )
    }

    makeRequest() {
        makeAPIrequest("GET", {
            action: "query",
            format: "json",
            prop: "revisions",
            titles: "Module:ItemList",
            rvprop: "content",
            rvslots: "main",
            rvlimit: "1"
        }).then((response) => {
            this.setState({content: parse(response.data.query.pages[Object.keys(response.data.query.pages)[0]].revisions[0].slots.main["*"])})  // :)
        }).catch(function (error) {
            console.log(error);
        })
    }


    filterData(filterPhrase: string) {
        const options = {
          includeScore: true
        }
        const fuse = new Fuse(Object.keys(this.state.content), options)
        const result = fuse.search(filterPhrase)
        return result.map(item => item.item);
    }

    setNewModalState() {
        this.setState({modal_open: false});
    }

    componentDidUpdate(prevProps: Readonly<MyProps>, prevState: Readonly<MyState>, snapshot?: any) {
        if (prevState.content !== this.state.content) {
            localStorageManager.items.overwrite(this.state.content);
            localStorageManager.items.save();
        }
    }

    changeFileProperties(file: string, url: string, previous_name: string) {
        const wiki_data: rawTable = this.state.content;
        wiki_data[previous_name].file = file.split(":", 2)[1];
        //wiki_data[previous_name].url = url;
        this.setState({content: wiki_data})
    }

    changeDataValue(key: string, field: string, value: string | boolean | number) {
        let currentData = this.state.content;
        if (value !== "") {
            currentData[key][field] = value;
        } else {
            delete currentData[key][field];
        }
        this.setState({content: currentData})
    }

    changeCurrencyStatus(key: string, currency: string, value: number) {
        let currentData = this.state.content;
        // For ease, normalize currencies and make an array of them
        let currenciesn: rawTable = {};
        if (currentData[key].currencies) {
            for (let one_currency of currentData[key].currencies) {
                currenciesn[one_currency.name] = one_currency.amount;
            }
        }
        if (currentData[key].currency) {
            currenciesn[currentData[key].currency.name] = currentData[key].currency.amount;
        }
        currenciesn[currency] = value;
        let translatedCurrencies: { name: string; amount: any; }[] = [];
        Object.entries(currenciesn).map(([key, value]) => translatedCurrencies.push({"name": key, "amount": value}))
        currentData[key]['currencies'] = translatedCurrencies;
        this.setState({content: currentData})
    }

    CurrencySelectBox(changeFun: (e: React.ChangeEvent<HTMLSelectElement>) => void, def_val: string) {
        return (
            <select name="Currencies" className="currency-name-selector" onChange={changeFun} defaultValue={def_val}>
                <option value="cane">Candy cane</option>
                <option value="candycorn">Candy corn</option>
                <option value="fruit basket">Fruit basket</option>
                <option value="easter egg">Easter egg</option>
                <option value="egg">Egg</option>
                <option value="envelope">Envelope</option>
                <option value="cheese coin">Cheese coin</option>
                <option value="carl coin">Carl coin</option>
                <option value="ticket">S.A.W. Ticket</option>
            </select>)
    }

    OneCurrency(currency: currency | null, data: itemData) {
        const changeCurrencyName = (e: React.ChangeEvent<HTMLSelectElement>) => {
            this.changeCurrencyStatus(data.name, currency == null ? "carl coin" : currency.name, 0);
            this.changeCurrencyStatus(data.name, e.target.value, parseInt(e.target.value));

        }
        if (currency === null) {
            return <div className="extension-currency">{this.CurrencySelectBox(changeCurrencyName, "carl coin")}<input className="extension-currency-amount" placeholder="Amount" onChange={(e) => {this.changeCurrencyStatus(data.name, "carl coin", parseInt(e.target.value))}} defaultValue={0}/></div>
        }
        return (
            <div className="extension-currency">{this.CurrencySelectBox(changeCurrencyName, currency.name)}<input className="extension-currency-amount" type="number" placeholder="Amount" onChange={(e) => {this.changeCurrencyStatus(data.name, currency.name, parseInt(e.target.value))}} defaultValue={currency.amount}/></div>
        )
    }

    ItemExtension(data: itemData) {
        let currency_list = [];
        if (data.currencies) {
            currency_list.push(data.currencies.map((currency) => this.OneCurrency(currency, data)));
        }
        currency_list.push(this.OneCurrency(null, data));
        if (!this.state.forced_currency_windows.includes(data.name) && undefined === data.currencies && undefined === data.currency && undefined === data.event) { // so, I'd simplify it but TypeScript seems unhappy about doing multiple variable comparison?
            return (
                <button className="extension-plus" onClick={(e) => {
                    if (!this.state.forced_currency_windows.includes(data.name)) {
                        console.log(this.state.forced_currency_windows.concat([data.name]));
                        this.setState({forced_currency_windows: this.state.forced_currency_windows.concat([data.name])});
                    }
                }}>+</button>
            )
        }
        return (
            <div className="extension-box">
                <div className="extension-other">
                    <h3 className="extension-header">Other</h3>
                    <div className="event"><input type="text" className="event-text" placeholder="Event" onChange={(e) => {this.changeDataValue(data.name, "event", e.target.value)}} defaultValue={data.event}/></div>
                    <div className="bundle"><input type="text" className="bundle-text" placeholder="Bundle name" onChange={(e) => {this.changeDataValue(data.name, "bundle", e.target.value)}} defaultValue={data.bundle}/></div>
                    <div className="shop"><input type="text" className="shop" placeholder="Shop" onChange={(e) => {this.changeDataValue(data.name, "shop", e.target.value)}} defaultValue={data.shop}/></div>
                </div>
                <div className="extension-currencies">
                    <h3 className="extension-header">Currencies</h3>
                    {currency_list}
                </div>
            </div>
        )
    }

    ItemCard(data: itemData) {
        return (
            <div className={"ui card " + getColor(data.rarity)} key={data.name}>
                <button disabled={this.state.brokenImgs[data.name] === undefined} className="invisible" onClick={(e) => {
                    this.setState({selectedImg: data.name, modal_open: true});
                }}><div className="image"><img alt={data.name} onError={(e) => {
                    let currently_broken = this.state.brokenImgs;
                    currently_broken[data.name] = false;
                    // @ts-ignore
                    e.target.src = failed;
                    this.setState({brokenImgs: currently_broken});
                }} referrerPolicy="no-referrer" src={data.image_url}/></div></button>
                <div className="content">
                    <div className="header"><span className="item-name"/>{data.name}</div>
                    <div className="header2"><input defaultValue={data.custom_name} disabled={false} placeholder="Custom name" onChange={(e) => {this.changeDataValue(data.name, "name", e.target.value)}} className="item-name-custom"/></div>
                    <div className="description"><textarea className="item-description" placeholder="Description" onChange={(e) => {this.changeDataValue(data.name, "description", e.target.value)}} defaultValue={data.description}/></div>
                    <div className="hidden"><input type="checkbox" defaultChecked={data.hidden} onChange={(e) => {this.changeDataValue(data.name, "hidden", e.target.checked)}}/>Hidden</div>
                    <div className="link"><input className="item-link" placeholder="Link" onChange={(e) => {this.changeDataValue(data.name, "link", e.target.value)}} defaultValue={data.link}/></div>
                    <div className="group"><input type="number" className="group-num" placeholder="Group (sorting key)" onChange={(e) => {this.changeDataValue(data.name, "group", parseInt(e.target.value))}} defaultValue={data.group}/></div>
                    <div className="extended">{this.ItemExtension(data)}</div>
                </div>
            </div>
        )
    }

    prepareCards(indexStart: number = 0, indexEnd: number = 100) {
        const wiki_data: rawTable = this.state.content;
        let keys = this.props.term ? this.filterData(this.props.term) : Object.keys(wiki_data).reverse();
        let output: JSX.Element[] = [];
        for (let i = indexStart; i < indexEnd;i++) {
            let key = keys[i];
            if (key === undefined) {
                continue;
            }
            let value = wiki_data[key];
            if (["ExpBoost"].includes(value.class)) {
                continue;
            }
            let file_name = value.file || (key+".png").replace(" ", "_");
            let file_url = `https://animalroyale.fandom.com/wiki/Special:FilePath/${file_name}`
            output.push(this.ItemCard({name: key, custom_name: value.name, rarity: value.rarity, image_url: file_url, class: value.class, link: value.link, description: value.description, extra: "", hidden: value.hidden, group: value.group}))
        }
        output.push(<SearchItem button={this.state.modal_open} itemName={this.state.selectedImg} setNewState={this.setNewModalState} setNewFile={this.changeFileProperties}/>)
        return output
    }

    render() {
        if (!this.state.content) {
            return (
                <Dimmer active>
                    <Loader/>
                </Dimmer>
            )
        } else {
            return this.prepareCards(0, this.state.load_items)
        }
    }
}

export default ItemGallery;