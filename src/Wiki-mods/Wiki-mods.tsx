import "./Wiki-mods.css";
import React from "react";
import { Segment } from 'semantic-ui-react'
import ItemGallery from "./Gallery";
import {Link} from "react-router-dom";
import ExportLuaModule from "./exportModule";


interface MyProps {
}

interface MyState {
    term: string,
    modalState: boolean
}

class WikiMods extends React.Component<MyProps, MyState> {
    constructor(prop: any) {
        super(prop);
        this.state = {
            term: "",
            modalState: false
        };
        this.getModalClose = this.getModalClose.bind(this);
    }

    getModalClose() {
        this.setState({modalState: false})
    }

    render() {
        return (
            <div>
                <div className="header-mods">
                    <div className="header-mods-name"><Link to="/"><button className="ui button">SAR Wiki Toolset</button></Link></div>
                    <div className="header-search">
                        <input className="input" placeholder="Enter item name" value={this.state.term} onChange={e => this.setState({term: e.target.value})}/>
                    </div>
                    <div>
                        <button className="ui positive button" onClick={(e) => this.setState({modalState: true})}>Save data</button>
                    </div>
                </div>
                <div className="main-content">
                    <Segment className="item-gallery">
                        <ItemGallery term={this.state.term}/>
                    </Segment>
                </div>
                <ExportLuaModule getModalClose= {this.getModalClose} currentState={this.state.modalState}/>
            </div>
        )
    }
}

export default WikiMods;
