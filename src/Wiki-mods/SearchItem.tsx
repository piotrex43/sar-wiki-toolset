import React from "react";
import { Button, Modal} from 'semantic-ui-react';
import makeAPIrequest from "../MediaWikiRequests";

interface Props {
    button: boolean,
    itemName: string,
    setNewState(): void,
    setNewFile(file: string, url: string, previous_name: string): void
}


interface itemData {
    url: string,
    fileName: string,
    comment: string,
    user: string,
    currentName: string,
    close(): void,
    passName(file: string, url: string, previous_name: string): void
}

const item = (data: itemData) => {
    return (
        <div className="item" onClick={(e) => {
            data.passName(data.fileName, data.url, data.currentName);
            data.close()
        }} key={data.fileName}>
            <div className="image"><img referrerPolicy="no-referrer" src={data.url}/></div>
            <div className="content">
                <div className="header">{data.fileName}</div>
                <div className="meta">{data.user}</div>
                <div className="description">{data.comment}</div>
            </div>
        </div>
    )
}

const SearchItem = (props: Props) => {
    const [open, setOpen] = React.useState(false);
    const [possibleFiles, setpossibleFiles] = React.useState([]);
    const [searchedFile, setsearchedFile] = React.useState(props.itemName);

    const openModal = () => {
        props.itemName = "";
        setOpen(true);
    }

    const makeRequest = (name: string) => {
        makeAPIrequest("GET", {
            action: "query",
            format: "json",
            prop: "imageinfo",
            generator: "search",
            iiprop: "timestamp|user|url|comment",
            gsrsearch: name,
            gsrnamespace: "6",
            gsrlimit: '10'
        }).then((result) => {
            let item_object: Array<object> = Object.values(result.data.query.pages);
            let final_product = [];
            for (const page of item_object) {
                // @ts-ignore
                final_product.push(item({fileName: page.title, url: page.imageinfo[0].url, comment: page.imageinfo[0].comment, user: page.imageinfo[0].user, passName: props.setNewFile, currentName: props.itemName, close: props.setNewState}))
            }
            // @ts-ignore
            setpossibleFiles(final_product);
        }).catch((error) => {
            console.log(error);
        })
    }

    React.useEffect(() => {
        if (open) {
            makeRequest(props.itemName);
        }
    }, [open]);

    React.useEffect(() => {
      if(props.button) {
          setOpen(true);
      } else {
          setOpen(false);
      }
    }, [makeRequest]);

    return (
        <Modal
          onClose={() => {setOpen(false);
                            props.setNewState();}}
          onOpen={openModal}
          open={open}
        >
      <Modal.Header>Find image on wiki</Modal.Header>
      <Modal.Description>
          This menu is used to find an image on the wiki to quickly add it as a custom one for the module.
      </Modal.Description>
            <Modal.Content>
                    <div className="ui small label">
                        Search for a file
                    </div>
                    <br/>
                    <div className="ui input">
                        <input type="text" placeholder="File name" id="fileName" defaultValue={props.itemName} onChange={(e) => {
                            setsearchedFile(e.target.value);
                        }} onKeyDown={(e) => {
                            if (e.key === "Enter") {
                                makeRequest(searchedFile);
                            }
                        }}/>
                    </div>
                <div className="ui items">
                    {possibleFiles}
                </div>
            </Modal.Content>
      <Modal.Actions>
        <Button color='black' onClick={props.setNewState}>
          Close window
        </Button>
      </Modal.Actions>
    </Modal>
    )
}

export default SearchItem;