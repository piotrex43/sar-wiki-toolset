import React from "react";
import localStorageManager from "../localStorageManager";
import {Button, Modal} from "semantic-ui-react";
const { format } = require("lua-json");

interface Props {
    getModalClose(): void,
    currentState: boolean
}

const url = `https://animalroyale.fandom.com/${localStorageManager.settings.read().language === "en" || localStorageManager.settings.read().language === undefined ? "" : `${localStorageManager.settings.read().language}/`}wiki/Module:ItemList?action=edit`

const ExportLuaModule = (props: Props) => {
    //const [open, setOpen] = React.useState(true);

    return (
        <Modal
          onClose={() => {
              props.getModalClose();}}
          open={props.currentState}
        >
      <Modal.Header>Export module to the wiki</Modal.Header>
      <Modal.Description>
          Unfortunately the process of saving the module can't be automated. This application generates the module content however it still needs to be copy-pasted manually. It's a very simple process however and should be fairly easy to do. All you have to do is copy the content of code below inside of text box and paste it into the page linked below. Save the page on the wiki and everything should be good.
      </Modal.Description>
            <Modal.Content>
                    <div className="ui big label blue">
                        Module content
                    </div>
                    <br/>
                <textarea className="bigInput" value={format(localStorageManager.items.read(), {singleQuote: false, spaces: "\t"})} readOnly={true}/>
                <div className="redirector">
                    <a href={url} target="_blank" rel="noreferrer">Paste it in here (opens in a new tab)</a>
                </div>
            </Modal.Content>
      <Modal.Actions>
        <Button color='black' onClick={(e) => props.getModalClose()}>
          Close window
        </Button>
      </Modal.Actions>
    </Modal>
    )
}

export default ExportLuaModule;
