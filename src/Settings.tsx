import React from "react";
import { Button, Modal, Dropdown } from 'semantic-ui-react';
import localStorageManager from "./localStorageManager";

interface Props {
   button: JSX.Element;
}

const Settings = (props: Props) => {
    const [open, setOpen] = React.useState(false);
    const [playfabApi, setPlayfabApi] = React.useState("");
    const [language, setlanguage] = React.useState<any>("");
    // const [wikiLogin, setwikiLogin] = React.useState("");
    // const [wikiPassword, setwikiPassword] = React.useState("");

    const WikiLangs = [{ text: "English", value: "en" }, { text: "Spanish", value: "es" },
        { text: "Russian", value: "ru" },{ text: "Chinese", value: "zh"}, {text: "German", value: "de"}]

    // @ts-ignore
    const DropdownExampleSearchSelectionTwo = () => (
        <Dropdown placeholder='Language' search selection id="language" options={WikiLangs} value={language} onChange={(e, data) => setlanguage(data.value)}/>
    )

    const saveData = () => {
        let settings = {
            playfabApi: playfabApi,
            language: language
        }
        localStorageManager.settings.overwrite(settings);
        localStorageManager.settings.save();
        setOpen(false);
    }

    const openModal = () => {
        let settings = localStorageManager.settings.read();
        setPlayfabApi(settings.playfabApi);
        setlanguage(settings.language);
        setOpen(true);
    }

    const wipeSettings = () => {
        localStorageManager.settings.overwrite(localStorageManager.settings.assign_default());
        setOpen(false);
    }

    return (
        <Modal
          onClose={() => setOpen(false)}
          onOpen={openModal}
          open={open}
          trigger={props.button}
        >
      <Modal.Header>Settings</Modal.Header>
      <Modal.Description>
          All settings are saved locally, they are never sent to any server and are stored in your browser's localStorage. Please be safe!
      </Modal.Description>
            <Modal.Content>
                    <div className="ui small label">
                        PlayFab API key
                    </div>
                    <br/>
                    <div className="ui input">
                        <input type="text" placeholder="API key" id="playfabApi" value={playfabApi} onChange={(e) => setPlayfabApi(e.target.value)}/>
                    </div><br/>
                    <div className="ui small label">
                        Wiki language
                    </div><br/>

                    {DropdownExampleSearchSelectionTwo()}
            </Modal.Content>
      <Modal.Actions>
        <Button
          color='red'
          content="Wipe settings"
          labelPosition='right'
          icon='x'
          onClick={wipeSettings}

        />
        <Button color='black' onClick={() => setOpen(false)}>
          Discard settings
        </Button>
        <Button
          content="Save settings"
          labelPosition='right'
          icon='checkmark'
          onClick={saveData}
          positive
        />
      </Modal.Actions>
    </Modal>
    )
}

export default Settings;