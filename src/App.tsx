import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Home from "./Home";
import WikiMods from "./Wiki-mods/Wiki-mods";
import Playfab from "./Playfab/Playfab";

function App() {
  return (
    <Router basename="/sar">
        <div className="App">
            <Switch>
                <Route path="/wiki-data">
                    <WikiMods/>
                </Route>
                <Route path="/playfab">
                    <Playfab/>
                </Route>
                <Route path="/">
                    <Home/>
                </Route>
            </Switch>
        </div>
    </Router>
  );
}

export default App;
