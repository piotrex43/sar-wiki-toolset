interface defaultSettings {
    playfabApi: string,
    language: string
}

interface rawTable {
    [index: string]: any
}

interface date {

}

class Settings {
    storage: defaultSettings;
    constructor() {
        let settings = localStorage.getItem("settings");
        if (settings === undefined || settings === null) {
            this.storage = this.assign_default();
            this.save();
        } else {
            this.storage = JSON.parse(settings);
        }
    }
    assign_default(): defaultSettings {
        return {
            playfabApi: "",
            language: ""
        }
    };
    read(): defaultSettings {
        return this.storage;
    }
    overwrite(newContent: defaultSettings): void {
        this.storage = newContent;
    }
    save(): void {
        localStorage.setItem("settings", JSON.stringify(this.storage));
    }
}

let settingsController = new Settings();

class ItemData {
    storage: object;
    constructor() {
        let data = localStorage.getItem("itemdata");
        if (data === undefined || data === null) {
            this.storage = this.assign_default();
            this.save();
        } else {
            this.storage = JSON.parse(data);
        }
    }
    assign_default(): {} {
        return {}
    };
    read(): any {
        return this.storage;
    }
    overwrite(newContent: rawTable): void {
        this.storage = newContent;
    }
    save(): void {
        localStorage.setItem("itemdata", JSON.stringify(this.storage))
    }
}

let itemController = new ItemData();

class UpdateDate {
    storage: date;
    constructor() {
        let data = localStorage.getItem("updateDate");
        if (data === undefined || data === null) {
            this.storage = this.assign_default();
            this.save();
        } else {
            this.storage = JSON.parse(data);
        }
    }
    assign_default(): {} {
        return new Date();
    };
    read(): date {
        return this.storage;
    }
    overwrite(): void {
        this.storage = new Date();
        this.save();
    }
    save(): void {
        localStorage.setItem("updateDate", JSON.stringify(this.storage))
    }
}

let lastUpdateDate = new UpdateDate();

export default {settings: settingsController, items: itemController, updateDate: lastUpdateDate};